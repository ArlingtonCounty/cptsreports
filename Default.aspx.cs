﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using System.IO;
using ClosedXML;
using ClosedXML.Excel;
/*------------------------------------------------------------------------------------------------------/
| Purpose			    : Pulls Daily CPTS Tocket data based on Range 
| Date					: 03/15/2018
| Author				: nkatoch
/------------------------------------------------------------------------------------------------------*/
public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack & cont>0 & TextBoxStartDate.Text != "" & TextBoxEndDate.Text != "")
        {
            string script = "$(document).ready(function () { $('[id*=ButtonView]').click(); });";
            ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);
        }

    }

    int cont = 0;
    private void CreateTable()
    {
        
            //Populating a DataTable from database.
            DataTable dt = GetData(TextBoxStartDate.Text, TextBoxEndDate.Text);
        if (TextBoxStartDate.Text!= "" & TextBoxEndDate.Text != "") { cont = 1; }
            //Building an HTML string.
            StringBuilder html = new StringBuilder();

            //Table start.
            html.Append("<table id='Table1' border = '1'>");

            //Building the Header row.
            html.Append("<tr>");
            foreach (DataColumn column in dt.Columns)
            {
                html.Append("<th>");
                html.Append(column.ColumnName);
                html.Append("</th>");
            }
            html.Append("</tr>");

            //Building the Data rows.
            foreach (DataRow row in dt.Rows)
            {
                html.Append("<tr>");
                foreach (DataColumn column in dt.Columns)
                {
                    html.Append("<td>");
                    html.Append(row[column.ColumnName]);
                    html.Append("</td>");
                }
                html.Append("</tr>");
            }

            //Table end.
            html.Append("</table>");

            //Append the HTML string to Placeholder.
            PlaceHolderTable.Controls.Add(new Literal { Text = html.ToString() });
        
    }
    private DataTable GetData(string startDate,string endDate)
    {
        string constr = ConfigurationManager.ConnectionStrings["CPTSConnectionString"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("getTicketData"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@startDate", startDate));
                cmd.Parameters.Add(new SqlParameter("@endDate", endDate));

                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                         return dt;
                    }
                }
            }
        }
    }

    protected void ButtonView_Click(object sender, EventArgs e)
    {
        CreateTable();
    }

    

    public void ExportToExcel(DataTable dt, string SheetName = "")
    {
        using (XLWorkbook wb = new XLWorkbook())
        {
            if (SheetName == "") { wb.Worksheets.Add("CPTS Ticket Export"); } else { wb.Worksheets.Add(SheetName); }
            wb.Worksheet(1).Cell(1, 1).InsertTable(dt);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=CPTS Ticket Export"  + ".xlsx");
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                wb.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }
    }


    protected void ButtonExport_Click(object sender, EventArgs e)
    {
        HiddenFieldShow.Value = "No";
        DataTable dt = GetData(TextBoxStartDate.Text, TextBoxEndDate.Text);
        ExportToExcel(dt);
       
    }
}


