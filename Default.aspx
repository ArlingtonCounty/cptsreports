﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="Scripts/ScrollableTablePlugin_1.0_min.js"></script>
    <script type="text/javascript">
    $(function () {
        $('#Table1').Scrollable({
            ScrollHeight: 800
        });
    });

    
   
    function ShowProgress() {
    
        setTimeout(function () {
            var modal = $('<div />');
            modal.addClass("modal");
            $('body').append(modal);
            var loading = $(".loading");
            loading.show();
            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
            loading.css({ top: top, left: left });
        }, 200);
    }
    $('form').live("submit", function () {
        
        if (Page_IsValid && document.getElementById('<%= HiddenFieldShow.ClientID%>').value != "No" ) 
        { ShowProgress(); }
        

        
    });

            function setShowYes()
    {
        document.getElementById('<%= HiddenFieldShow.ClientID%>').value = 'Yes';
      
    }
        function setShow()
    {
        document.getElementById('<%= HiddenFieldShow.ClientID%>').value = 'No';
      
    }
</script>
     <style type="text/css">
    .rfv
    {
        padding-left:10px;
        color:#B50128;
        font-size:12px;
        font-family: Verdana, Tahoma, Arial;
        font-weight:bold;
    }

     .modal
    {
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }
    .loading
    {
        font-family: Arial;
        font-size: 10pt;
        border: 5px solid #67CFF5;
        width: 200px;
        height: 100px;
        display: none;
        position: fixed;
        background-color: White;
        z-index: 999;
    }
    tdCenter {
    text-align: center; /* center textbox horizontally */
    vertical-align: middle; /* center textbox vertically */
    
}
    </style>
</head>

<body>
    <form id="form1" runat="server">

        <asp:scriptmanager runat="server"></asp:scriptmanager>
    <div>
    <table border = '1' style="margin: 0px auto;width:auto">
        <tr>
            <td class="tdCenter" >Start Date(MM/DD/YYYY)</td>
            <td class="tdCenter" >
               <asp:TextBox ID="TextBoxStartDate" runat="server" TextMode="Date" Width="150px" Height="20px"></asp:TextBox>
               <asp:CompareValidator    id="dateValidatorStart" runat="server" CssClass="rfv"   Type="Date"  Operator="DataTypeCheck"  ControlToValidate="TextBoxStartDate"  ErrorMessage="Please enter a valid date."> </asp:CompareValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="rfv" ControlToValidate="TextBoxStartDate" ErrorMessage="Enter Valid Start Date"></asp:RequiredFieldValidator>
            </td>
            
        </tr>
        
                <tr>
            <td class="tdCenter" >End Date(MM/DD/YYYY)</td>
             <td class="tdCenter">
                <asp:TextBox ID="TextBoxEndDate"  runat="server" Width="150px" TextMode="Date"  Height="20px"></asp:TextBox>
       <asp:CompareValidator    id="dateValidatorEnd" runat="server"   Type="Date"  Operator="DataTypeCheck" CssClass="rfv"  ControlToValidate="TextBoxEndDate"  ErrorMessage="Please enter a valid date."> </asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="TextBoxEndDate" CssClass="rfv" runat="server" ErrorMessage="Enter Valid End Date"></asp:RequiredFieldValidator>
                    </td>
        </tr>
    </table>
    </div>
        <div style="float:right"> <asp:Button ID="ButtonView" runat="server" Text="View Report" OnClientClick="setShowYes();" OnClick="ButtonView_Click" /> <asp:Button ID="ButtonExport" runat="server" Text="Export Report" OnClientClick="setShow();" OnClick="ButtonExport_Click" /></div>
        <asp:HiddenField ID="HiddenFieldShow" Value="Yes" runat="server" />
      <br />
        <br />
  <div>
      <asp:PlaceHolder ID = "PlaceHolderTable" runat="server" />
  </div>

      <div class="loading" align="center">
    Loading. Please wait.<br />
    <br />
<img src="loader.gif" />
</div>
    </form>
</body>
</html>
